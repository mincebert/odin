; helper file to assemble "t_odin.asm" with sjasmplus (to get binary for comparison)
; to build: sjasmplus t_odin_sj.asm

    OPT --zxnext --syntax=abf               ; MUL will warn about being fake, because missing "de" arg
    DEFINE sl1 sli
    DEFINE swap swapnib
    DEFINE mirr mirror
    DEFINE otib outinb
    DEFINE nreg nextreg
    DEFINE pxdn pixeldn
    DEFINE pxad pixelad
    DEFINE stae setae
    DEFINE lirx ldirx
    DEFINE lprx ldpirx
    DEFINE ldrx lddrx
    DEFINE defc dc
    DEFINE defz dz
    MACRO save name?,start?,size? : ENDM    ; empty macro to avoid odin's SAVE

    OUTPUT "_t_odin_sj.bin.tmp"
        INCLUDE "t_odin.asm"
    OUTEND
    DISPLAY "[ASSEMBLING TEST] Expected binary size (_t_odin.bin.tmp): ",/A,End-Start
