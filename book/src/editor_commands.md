# Editor commands

## Single command keys

| Key        | Description                  |
|------------|------------------------------|
| EDIT       | Exit editor                  |
| TRUE VIDEO | Move to next field           |
| LEFT       | Move cursor left             |
| DOWN       | Move cursor to next line     |
| UP         | Move cursor to previous line |
| RIGHT      | Move cursor right            |
| DELETE     | Delete previous character    |
| ENTER      | Insert new line below        |

## Extended commands

These are keys that are pressed in conjunction with `EXTENDED MODE`.

| Key            | Description                   |
|----------------|-------------------------------|
| EXT+ENTER      | Insert new line above         |
| EXT+EDIT       | Switch file                   |
| EXT+CAPS       | -                             |
| EXT+TRUE VIDEO | Beginning of file             |
| EXT+INV VIDEO  | End of file                   |
| EXT+LEFT       | Move to start of line         |
| EXT+DOWN       | Move one page down            |
| EXT+UP         | Move one page up              |
| EXT+RIGHT      | Move to end of line           |
| EXT+GRAPH      | -                             |
| EXT+DELETE     | Delete character under cursor |
| EXT+A          | -                             |
| EXT+B          | -                             |
| EXT+C          | Copy current line/block       |
| EXT+D          | Duplicate current line/block  |
| EXT+E          | Delete to end                 |
| EXT+F          | -                             |
| EXT+G          | -                             |
| EXT+H          | -                             |
| EXT+I          | -                             |
| EXT+J          | -                             |
| EXT+K          | -                             |
| EXT+L          | -                             |
| EXT+M          | -                             |
| EXT+N          | -                             |
| EXT+O          | Toggle overwrite mode         |
| EXT+P          | -                             |
| EXT+Q          | -                             |
| EXT+R          | -                             |
| EXT+S          | Save current file             |
| EXT+T          | -                             |
| EXT+U          | -                             |
| EXT+V          | Paste line/block              |
| EXT+W          | Close current document        |
| EXT+X          | Cut line/block                |
| EXT+Y          | -                             |
| EXT+Z          | -                             |
| EXT+SPACE      | Enter block mode              |
