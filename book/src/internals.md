# Odin Internals

This chapter describes the internals of Odin so that a reader can understand the
source code of Odin.

# Note about code references

Throughout this manual, code will be referenced in the pattern
`<source-path>:<code label>`. The source path may contain folders too. For
example, `asm/emit.s:emitWord` refers to the `emitWord` label in the file found
at `src/asm/emit.s` in the repository. `src` is omitted as it is common to all
source code files.
