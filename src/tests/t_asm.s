;;----------------------------------------------------------------------------------------------------------------------
; asm.s tests
; coverage: AsmContext.IncLineNum, asm8Bit (50%: non-memory reference covered), asmCC, ldGet8 (partial), asmCheckSeq

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__test_asm_s_bytes_start:

__t_asm_AsmContext      AsmContext
__t_asm_r8tok1  DB      OP_A,OP_B,OP_C,OP_D,OP_E,OP_H,OP_L
                ASSERT high(__t_asm_r8tok1) == high($)   ; asmCheckSeq does only `inc e`, not `inc de`, so fit into 256B
__t_asm_r8tok2  DB      OP_IXL,OP_IXH,OP_IYL,OP_IYH
__t_asm_cc_data DB      OP_NZ,$00,OP_Z,$08,OP_NC,$10,OP_C,$18,OP_PO,$20,OP_PE,$28,OP_P,$30,OP_M,$38
.NN = ($-__t_asm_cc_data)/2
__t_asm_ld_r8   DB      OP_A,R8_A,OP_C,R8_C,OP_B,R8_B,OP_E,R8_E,OP_D,R8_D,OP_L,R8_L,OP_H,R8_H
                DB      OP_IXL,R8_IXL,OP_IXH,R8_IXH,OP_IYL,R8_IYL,OP_IYH,R8_IYH,OP_R,R8_R,OP_I,R8_I
.NN = ($-__t_asm_ld_r8)/2
                ASSERT high(__t_asm_ld_r8) == high($)   ; routine does only `inc e`, not `inc de`, so fit into 256B
__t_asm_ld_m8   DB      '(',OP_HL,')',R8_HL,'(',OP_BC,')',R8_BC,'(',OP_DE,')',R8_DE,'(',OP_IX,')',R8_IX,'(',OP_IY,')',R8_IY
.NN = ($-__t_asm_ld_m8)/4
                ASSERT high(__t_asm_ld_m8) == high($)   ; routine does only `inc e`, not `inc de`, so fit into 256B

;;----------------------------------------------------------------------------------------------------------------------
; tests

__test_asm_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Asm: "
                ; page in assembler module
                ld      a,(AsmCode)
                page    7,a

                ; TEST AsmContext.IncLineNum
                ld      iy,__t_asm_AsmContext
                ld      hl,0
                ld      (__t_asm_AsmContext.LineNum),hl
                __T_SETUP 'L'
                call    AsmContext.IncLineNum
                __T_WORDCMP __t_asm_AsmContext.LineNum, 1
                __T_CHECK regs
                ld      hl,$00FF
                ld      (__t_asm_AsmContext.LineNum),hl
                __T_SETUP '.'
                call    AsmContext.IncLineNum
                __T_WORDCMP __t_asm_AsmContext.LineNum, $0100
                __T_CHECK regs
                __T_SETUP '.'
                call    AsmContext.IncLineNum
                __T_WORDCMP __t_asm_AsmContext.LineNum, $0101
                __T_CHECK regs

                ; TEST asm8Bit
                xor     a
                ld      (Pass),a
                ld      hl,$1234
                ld      (PC),hl
                ld      hl,$3456
                ld      (WritePC),hl
                __T_SETUP '8'       ; A reg
                ld      de,__t_asm_r8tok1
                ld      a,(de)
                call    asm8Bit
                ld      hl,__t_default_hl
                __T_CHECK ncf_zf_a_de_bc : db OP_A : dw __t_asm_r8tok1+1, 7|(__t_default_bc&$FF00)
__t_asm_asm8Bit_r8:
                __T_SETUP 'b'       ; B, C, D, E, H, L
                ld      b,0
                ld      de,__t_asm_r8tok1+1
.next:
                ld      a,(de)
                call    asm8Bit
                jr      c,.fail
                jr      nz,.fail
                ld      a,b
                inc     b
                cp      c
                jr      nz,.fail
                cp      5
                jr      nz,.next
                xor     a
                ld      hl,__t_default_hl
.fail:          __T_CHECK ncf_zf_a_de_bc : db 0 : dw __t_asm_r8tok1+7, $0605
                __T_SETUP '.'       ; A = 0 (not 8bit argument)
                ld      de,1
                xor     a
                call    asm8Bit
                ld      hl,__t_default_hl
                __T_CHECK only_ncf_nzf_a_de : db 0 : dw 1
__t_asm_asm8Bit_r8_ixy:
                ld      ix,$0000
                __T_SETUP ':'       ; IXL/IXH/IYL/IYH
                ld      de,__t_asm_r8tok2
                ld      b,4
.l0:
                ld      a,(de)
                call    asm8Bit
                jr      nc,.fail
                jr      nz,.fail
                dec     de
                ld      a,(de)
                cp      h           ; asm8Bit returns in H the opcode in case of index r8
                jr      nz,.fail
                inc     de
                add     ix,bc
                djnz    .l0
                ld      bc,ix
                ld      hl,__t_default_hl
                xor     a
                scf                 ; set zf+cf to 1, and A to 0 for final "non-fail" check
.fail:
                __T_CHECK cf_zf_a_de_bc : db 0 : dw __t_asm_r8tok2+4, ($0405+$0304+$0205+$0104)&$FFFF

                ; TEST asmCC
__t_asm_asmCC_test:
                __T_SETUP 'c'
                ld      hl,__t_asm_cc_data
                ld      b,__t_asm_cc_data.NN
                ld      de,$ffff    ; -1 in E, will be counter of correct matches x2
.l0:            ldi     a,(hl)
                call    asmCC
                jr      c,.fail
                cp      (hl)
                jr      nz,.fail
                inc     e
                inc     hl
                djnz    .l0
.fail:          ; in case of fail just continue, the DE should mis-match in final test
                ld      a,OP_R
                call    asmCC       ; should do CF=1 and keep DE
                ex      de,hl       ; HL = final DE to check with available "cf_a_hl" check
                ld      de,__t_default_de
                ld      bc,__t_default_bc
                __T_CHECK cf_a_hl : db OP_R : dw $ff00 + __t_asm_cc_data.NN*2 - 1

                ; TEST ld.s ldGet8
__t_asm_ldGet8_test:
                __T_SETUP 'r'       ; loop through all possible token values $80..$FF and count how many matched
                ld      a,$80
.l0:            push    af
                call    ldGet8
                sbc     a,a
                inc     a           ; 0 = non match, 1 = match
                add     hl,a        ; count number of matched tokens
                pop     af          ; ldGet8 will also advance DE upon every match
                inc     a
                jr      nz,.l0
                ld      bc,__t_default_bc       ; revert BC to pass check
                __T_CHECK a_hl_de : db 0 : dw __t_default_hl+13, __t_default_de+13
__t_asm_ldGet8_test_r8:
                __T_SETUP '8'       ; loop through all valid simple R8 tokens from the table, check also resulting C
                ld      de,__t_asm_ld_r8
                ld      b,__t_asm_ld_r8.NN
.l0:            ld      a,(de)      ; token to check (simple R8 tokens only, no memory ref or value)
                call    ldGet8
                jr      c,.fail
                ld      a,(de)
                cp      c
                jr      nz,.fail
                inc     e
                djnz    .l0                     ; final A should be R8_I
                ld      bc,__t_default_bc       ; revert BC to pass check
.fail:          __T_CHECK a_de : db R8_I : dw __t_asm_ld_r8 + 2*__t_asm_ld_r8.NN
__t_asm_ldGet8_test_m8:
                __T_SETUP '.'       ; check the memory references (valid for r8 destination): (HL),(DE),(BC),(IX),(IY)
                ld      de,__t_asm_ld_m8        ; but without displacements or addresses
                ld      b,__t_asm_ld_m8.NN
.l0:            ld      a,(de)      ; first char (should be always opening parenthesis)
                call    ldGet8
                jr      c,.fail
                ld      a,(de)
                cp      c
                jr      nz,.fail
                inc     e
                djnz    .l0                     ; final A should be R8_IY
                ld      bc,__t_default_bc       ; revert BC to pass check, HL is 0 from (IX) and (IY) (tests only last)
.fail:          __T_CHECK a_hl_de : db R8_IY : dw 0, __t_asm_ld_m8 + 4*__t_asm_ld_m8.NN
                ; more complex cases of ldGet8 including memory, values and displacement
                ; are covered by the all_z80 integration test

                ; TEST asmCheckSeq
                __T_SETUP 's'
                ld      de,__t_asm_r8tok1
                call    asmCheckSeq
                dz      OP_A,OP_B,OP_C
                __T_CHECK ncf_a_de : db OP_D : dw __t_asm_r8tok1+3
                __T_SETUP '.'
                ld      de,__t_asm_r8tok1
                call    asmCheckSeq
                dz      OP_A,OP_C,$AF       ; $AF is `xor a` in case asmCheckSeq returns at first mismatch or +1 address
                __T_CHECK cf_a_de : db OP_B : dw __t_asm_r8tok1+1

                ; TEST asmOPT
                call    pageDebugger
                xor     a
                ld      (DebuggerPause),a
                ld      (Pass),a            ; only pass 0 is significant for OPT
                __T_SETUP 'o'
                __T_MEMCPY AsmBuffer, <T_SYMBOL, "PAUSEE\0", 0>
                ld      de,AsmBuffer
                call    asmOPT
                __T_CHECK errorPrintCnt1
                __T_SETUP '.'
                __T_MEMCPY AsmBuffer, <T_SYMBOL, "PAUS\0", 0>
                ld      de,AsmBuffer
                call    asmOPT
                __T_CHECK errorPrintCnt1
                __T_SETUP ':'
                xor     a
                ld      de,AsmBuffer
                ld      (de),a      ; empty input
                call    asmOPT
                __T_CHECK errorPrintCnt1
                __T_SETUP '.'
                __T_MEMCPY AsmBuffer, <T_STRING, "PAUSE\0", 0>
                ld      de,AsmBuffer
                call    asmOPT
                __T_CHECK errorPrintCnt1
                __T_SETUP ':'
                __T_MEMCPY AsmBuffer, <T_SYMBOL, "PAUSE\0", '-', 0> ; extra argument after PAUSE command -> error
                ld      de,AsmBuffer
                call    asmOPT
                __T_CHECK errorPrintCnt1
                __T_SETUP '.'
                __T_MEMCPY AsmBuffer, <T_SYMBOL, "pAuSe\0", 0>
                ld      de,AsmBuffer
                __T_BYTECMP DebuggerPause, 0
                call    asmOPT
                __T_BYTECMP DebuggerPause, 1
                __T_CHECK only_ncf_de : dw AsmBuffer+7

                ret

                display "[TEST RUN] tests: asm.s                        Space: ",/A,$-__test_asm_s_bytes_start
