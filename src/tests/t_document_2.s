;;----------------------------------------------------------------------------------------------------------------------
; document.s tests - part 2
; coverage: docExists, docFullList, docInfo, docSetName, docProcessOdnSize, docName

__test_document_s_2:
                ld a,7 : call colouredPrint : dz C_ENTER,"Document (2): "
                ; shared module must be already paged in (otherwise colouredPrint used above did crash)

                ;--------------------------------------------------------------------------------------------
                ; TEST docExists
                ld      hl,0
                ld      (MainDoc.Length),hl
                __T_SETUP 'e'
                call    docExists
                __T_CHECK cf
                ld      hl,1
                ld      (MainDoc.Length),hl
                __T_SETUP '.'
                call    docExists
                __T_CHECK ncf
                ld      hl,$100
                ld      (MainDoc.Length),hl
                __T_SETUP '.'
                call    docExists
                __T_CHECK ncf

                ;--------------------------------------------------------------------------------------------
                ; TEST docFullList
                ld      hl,0
                ld      (MainDoc + kNumDocs*Document),hl
                __T_SETUP 'F'
                call    docFullList
                __T_CHECK ncf
                ld      hl,1
                ld      (MainDoc + kNumDocs*Document),hl
                __T_SETUP '.'
                call    docFullList
                __T_CHECK cf
                ld      hl,$100
                ld      (MainDoc + kNumDocs*Document),hl
                __T_SETUP '.'
                call    docFullList
                __T_CHECK cf

                ;--------------------------------------------------------------------------------------------
                ; TEST docInfo
                __T_SETUP 'i'
                xor     a
                call    docInfo
                __T_CHECK a_hl : db 0 : dw MainDoc
                __T_SETUP '.'
                ld      a,1
                call    docInfo
                __T_CHECK a_hl : db 1 : dw OtherDocs
                __T_SETUP '.'
                ld      a,16
                call    docInfo
                __T_CHECK a_hl : db 16 : dw OtherDocs + 15*Document

                ;--------------------------------------------------------------------------------------------
                ; TEST docSetName
                xor     a
                ld      (FileName),a
                __T_SETUP 'n'           ; empty filename (\0)
                call    docSetName
                __T_BYTECMP MainDocName, 0
                __T_CHECK all

                __T_SETUP '.'           ; "test.odn"
                __T_STRCPY FileName, "test.odn"
                call    docSetName
                __T_MEMCMP FileName, "test.odn"
                __T_MEMCMP MainDocName, "test.odn"
                __T_CHECK all

                xor     a
                ld      (FileName+4),a
                __T_SETUP '.'           ; "test"
                call    docSetName
                __T_MEMCMP FileName, "test"
                __T_MEMCMP MainDocName, "test"
                __T_CHECK all

                ;--------------------------------------------------------------------------------------------
                ; TEST docProcessOdnSize
                __T_SETUP 'S'
                ld      de,1
                ld      hl,123          ; too large file (DE > 0)
                call    docProcessOdnSize
                __T_CHECK only_cf
                __T_SETUP '.'
                ld      de,-1
                ld      hl,123          ; too large file (DE > 0)
                call    docProcessOdnSize
                __T_CHECK only_cf
                __T_SETUP '2'
                ld      de,0
                ld      hl,$c001        ; too large file (HL > $c001)
                call    docProcessOdnSize
                __T_CHECK only_cf
                __T_SETUP '.'
                ld      de,0
                ld      hl,de           ; empty file
                call    docProcessOdnSize
                __T_CHECK only_cf
                ; valid sizes, should also calculate amount of pages occupied (to load/save/etc)
                __T_SETUP '3'
                ld      de,0
                ld      hl,1            ; 1 byte long file -> 1 page to load
                call    docProcessOdnSize
                __T_CHECK only_ncf_hl_bc : dw 1, $0100 | low __t_default_bc
                __T_SETUP '.'
                ld      de,0
                ld      hl,$2000        ; $2000 byte long file -> 1 page to load
                call    docProcessOdnSize
                __T_CHECK only_ncf_hl_bc : dw $2000, $0100 | low __t_default_bc
                __T_SETUP '.'
                ld      de,0
                ld      hl,$6001        ; $6001 byte long file -> 4 page to load
                call    docProcessOdnSize
                __T_CHECK only_ncf_hl_bc : dw $6001, $0400 | low __t_default_bc
                __T_SETUP '.'
                ld      de,0
                ld      hl,$c000        ; $c000 byte long file -> 6 page to load
                call    docProcessOdnSize
                __T_CHECK only_ncf_hl_bc : dw $c000, $0600 | low __t_default_bc

                ;--------------------------------------------------------------------------------------------
                ; TEST docName
                __T_SETUP 'N'
                page    4,$34
                xor     a
                call    docName
                __T_CHECK a_hl_bc : db 0 : dw MainDocName, $3400 | low __t_default_bc
                __T_SETUP '.'
                page    4,$35
                ld      a,1
                call    docName
                __T_CHECK a_hl_bc : db 1 : dw addrMMU4, $3500 | low __t_default_bc
                __T_SETUP '.'
                page    4,$36
                ld      a,kNumDocs
                call    docName
                __T_CHECK a_hl_bc : db kNumDocs : dw addrMMU4+(kNumDocs-1)*256, $3600 | low __t_default_bc

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

                display "[TEST RUN] tests: document.s (2)               Space: ",/A,$-__test_document_s_2
