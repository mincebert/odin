;;----------------------------------------------------------------------------------------------------------------------
;; Memory routines
;;----------------------------------------------------------------------------------------------------------------------

;                DEFINE MEM_DEBUG

;;----------------------------------------------------------------------------------------------------------------------
;; DMA memory routines
;;----------------------------------------------------------------------------------------------------------------------


memcpy_dma:
                db      %11000011       ; R6: Reset

                ; Register 0 set up
                db      %01111101       ; R0: A -> B, transfer mode
mc_dma_src:     dw      0               ; Source address
mc_dma_len:     dw      0               ; Length

                ; Register 1 set up (Port A configuration)
mc_dma_dir:     db      %01010100       ; R1: Port A config: increment, variable timing
                db      2               ; R1: Cycle length of 2 in port A

                ; Register 2 set up (Port B configuration)
mc_dma_dir2:    db      %01010000       ; R2: Port B config: address fixed, variable timing
                db      2               ; R2: Cycle length of 2 in port B

                ; Register 4 set up (Operation mode)
                db      %10101101       ; R4: Continuous mode, set destination address
mc_dma_dest:    dw      0               ; Destination address

                ; Register 5 set up (Some control)
                db      %10000010       ; R5: Stop at end of block; read active low

                ; Register 6 (Commands)
                db      %11001111       ; R6: Load
                db      %10000111       ; R6: Enable DMA


memcpy_len      EQU     $-memcpy_dma

memcpy:
        ; Input:
        ;       HL = source address
        ;       DE = destination address
        ;       BC = number of bytes
        ;
                push    af
                ld      a,b
                or      c
                jr      z,.no_bytes
                push    bc
                push    de
                push    hl
                ld      a,%01010100
.transfer:
                ld      (mc_dma_dir),a          ; Set direction
                xor     %00000100               ; %01010100->%01010000 | %01000100->%01000000
                ld      (mc_dma_dir2),a
                ld      (mc_dma_src),hl         ; Set up the source address
                ld      (mc_dma_len),bc         ; Set up the length
                ld      (mc_dma_dest),de        ; Set up the destination address
                ld      hl,memcpy_dma
                bchilo  memcpy_len,$6b
                otir                            ; Send DMA program
                pop     hl
                pop     de
                pop     bc
.no_bytes:
                pop     af
                ret

memmove:
        ; Input:
        ;       HL = source address
        ;       DE = destination address
        ;       BC = number of bytes
        ;
        ; Output:
        ;       CF = 0: normal copy, 1: reverse copy
        ;
        ; Destroys:
        ;       F
        ;
                call    compare16
                jr      nc,memcpy
                ;  |
                ; fallthrough into memcpy_r
                ;  |
                ;  v
memcpy_r:
        ; Input:
        ;       HL = source address
        ;       DE = destination address
        ;       BC = number of bytes
        ;
                push    af
                ld      a,b
                or      c
                jr      z,memcpy.no_bytes
                push    bc
                push    de
                push    hl

                ; Prepare the proper HL & DE
                add     hl,bc
                ex      de,hl
                add     hl,bc
                ex      de,hl
                dec     hl
                dec     de

                ; Set up the DMA
                ld      a,%01000100
                jr      memcpy.transfer

memclear:
        ; Input:
        ;       HL = destination address
        ;       BC = number of bytes
        ;
        ; Output:
        ;       A = 0, F set as by `xor a`
        ;
        ; fill BC bytes with zero at address HL (by memcpy)
        ;
                xor     a
                ;  |
                ; fallthrough into memfill
                ;  |
                ;  v
memfill:
        ; Input:
        ;       HL = destination address
        ;       BC = number of bytes
        ;       A = value to fill
        ;
        ; This is equivalent to memcpy(HL,HL+1,BC-1), when we load (HL) with A.
        ;
                push    af
                ld      a,b
                or      c
                jr      z,.done
                pop     af
                push    af
                push    de
                ld      (hl),a
                ld      d,h
                ld      e,l
                inc     de
                dec     bc
                call    memcpy
                inc     bc              ; Restore BC
                pop     de
.done           pop     af
                ret

memswap:
        ; Input:
        ;       HL = source 1 address
        ;       DE = source 2 address
        ;       BC = number of bytes
        ;
        ; Swaps two memory blocks of equal size
                push    af,bc,de,hl
.l0:
                ld      a,(de)          ; A = (DE)
                ldi                     ; (DE++) = (HL++), --BC, P/V=(0!=BC)
                dec     hl              ; --HL
                ld      (hl),a          ; (HL) = A
                inc     hl              ; ++HL
                jp      pe,.l0

                pop     hl,de,bc,af
                ret

memcmp:
        ; Input:
        ;       HL = address 1
        ;       DE = address 2
        ;       BC = number of bytes to compare
        ;
        ; Output:
        ;       when a byte differs:    ZF=0, A=first different byte from DE argument side
        ;       when all bytes match:   ZF=1, A=last matching byte
        ;       when BC==0:             ZF=1, A=0
        ;
                ld      a,b
                or      c
                ret     z
                push    bc,de,hl
.l0:            ldi     a,(de)
                cpi
                jr      nz,.end
                jp      pe,.l0
.end
                pop     hl,de,bc
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; Internal routine to call +3 DOS service (with bank 7)
;;
;; Input/Output: special, depends what service has to be called
;;
;;----------------------------------------------------------------------------------------------------------------------

callP3dos:
                exx                     ; Function parameters are switched to alternative registers.
                ld      de,IDE_BANK     ; Choose the function.
                ld      c,7             ; We want RAM 7 swapped in when we run this function (so that the OS can run).
                call    pageDivMMC
                rst     8
                db      M_P3DOS         ; Call the function
                jp      pageOutDivMMC

;;----------------------------------------------------------------------------------------------------------------------
;; Internal routine to allocate a single page from the OS.
;;
;; Output:
;;      A = page # (or 0 if failed)
;;      CF = 1 if out of memory
;;
;;----------------------------------------------------------------------------------------------------------------------

        IFNDEF allocPage                ; prevent real `allocPage` symbol to be defined, if test-runner stubs it
allocPage:
        ENDIF
                push    ix
                push    bc
                push    de
                push    hl

                ; Allocate a page by using the OS function IDE_BANK.
                ld      hl,$0001        ; Select allocate function and allocate from normal memory.
                call    callP3dos       ; Call the function, new page # is in E
        IFDEF MEM_DEBUG
                push    af
                sbc     a,a
                and     e               ; A = 0 if OOM or E if alloc success
                ld      hl,PagesAllocd
                add     hl,a
                ld      (hl),1
                pop     af
        ENDIF
                ccf
                ld      a,e
                pop     hl
                pop     de
                pop     bc
                pop     ix
                ret     nc
                xor     a               ; Out of memory, page # is 0 (i.e. error), CF = 1
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Internal routine to allocate N pages from the OS.
;;
;; Input:
;;      B = number of pages to allocate (1..255)
;;      HL = memory to store allocated-page numbers (byte array)
;;
;; Output:
;;      CF = 1 if out of memory (partially allocated pages are released, so result is all or nothing)
;;
;; Affects:
;;      AF
;;
;;----------------------------------------------------------------------------------------------------------------------

allocPages:
                push    bc,hl

            ; allocate N pages (exits early when OOM happens)
.allocate:
                call    allocPage
                ldi     (hl),a          ; page number or zero in case of OOM
                jr      c,.oom
                djnz    .allocate
.oom:
            ; return when all pages were allocated, otherwise enter freePages
                pop     hl,bc
                ret     nc
                ;  |
                ; fallthrough into freePages in case of OOM error
                ;  |
                ;  v
;;----------------------------------------------------------------------------------------------------------------------
;; Internal routine to deallocate N pages from the OS.
;;
;; Input:
;;      B = max number of pages to deallocate (1..255)
;;      HL = [zero terminated or B-item] array of allocated-page numbers
;;
;; Output:
;;      CF = 1 when the list was terminated earlier by zero page
;;      CF = 0 when B pages were released
;;
;; Affects:
;;      AF
;;
;;----------------------------------------------------------------------------------------------------------------------

freePages:
            ; deallocate B pages from HL array, but exit early when zero-page is found (null-terminator)
                push    bc,hl
.deallocate:
                ldi     a,(hl)
                cp      1               ; CF=1 when zero page is read from array
                jr      c,.nullPage
                call    freePage
                djnz    .deallocate
.nullPage:
                pop     hl,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; saveSlots
;; Store what pages are paged into which slots
;;

saveSlots:
                push    af,bc,hl
                ld      b,8
                ld      hl,SlotBuffer
.loop:
                ld      a,b
                dec     a
                call    readPage
                ldi     (hl),a
                djnz    .loop

                pop     hl,bc,af
                ret

SlotBuffer      ds      8

;;----------------------------------------------------------------------------------------------------------------------
;; restoreSlots
;; Restore the contents of the page slots stored with `saveSlots`.
;;

restoreSlots:
                push    af,bc,de,hl
                ld      b,8
                ld      de,SlotBuffer
                ld      hl,.page
.loop:
                ld      a,b
                add     a,$50-1
                ld      (hl),a
                ldi     a,(de)
.page+2         page    0,a
                djnz    .loop

                pop     hl,de,bc,af
                ret



;;----------------------------------------------------------------------------------------------------------------------
;; Internal routine to return a previously allocated page back to the OS.
;;
;; Input:
;;      A = page #
;;
;;----------------------------------------------------------------------------------------------------------------------

        IFNDEF freePage                 ; prevent real `freePage` symbol to be defined, if test-runner stubs it
freePage:
        ENDIF
                push    af
                push    ix
                push    bc
                push    de
                push    hl

        IFDEF MEM_DEBUG
                ld      hl,PagesAllocd
                add     hl,a
                ld      (hl),0
        ENDIF

                ld      e,a             ; E = page #
                ld      hl,$0003        ; Deallocate function from normal memory
                call    callP3dos

                pop     hl
                pop     de
                pop     bc
                pop     ix
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; insertSpace
;; Insert space into a buffer by moving data up.
;;
;; Input:
;;      HL = Address of insert point
;;      DE = End of buffer
;;      BC = Number of bytes to insert
;;
;; Output:
;;      CF = 1 when HL + BC points beyond end of buffer
;;

insertSpace:
                push    bc,de
                call    .getThreeArgs
                ; HL = input HL, DE = input HL + input BC, BC = bytes to move
                call    nc,memcpy_r
                pop     de,bc
                ret

.getThreeArgs:
    ; Input: HL = start adr, DE = end adr, BC = bytes to insert/remove
    ; Output: HL = HL, DE = HL + BC, BC = remaining bytes to end
    ;         CF=1 when HL+BC points beyond DE (end adr)
                push    hl
                add     hl,bc           ; HL = HL + BC ; should never overflow (CF=0)
                ex      de,hl
                sbc     hl,de           ; CF=1 when HL+BC points beyond end adr
                ld      bc,hl           ; BC = number of bytes to move = input (DE-(HL+BC))
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; removeSpace
;; Remove space from buffer by copying data at end over remove point
;;
;; Input:
;;      HL = Address of remove point
;;      DE = End of buffer
;;      BC = Number of bytes to remove
;;
;; Output:
;;      CF = 1 when HL + BC points beyond end of buffer
;;

removeSpace:
                push    bc,de
                call    insertSpace.getThreeArgs
                ; HL = input HL, DE = input HL + input BC, BC = bytes to move
                ex      de,hl           ; DE = destination, HL = source for move
                call    nc,memcpy
                ex      de,hl           ; Restore HL
                pop     de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Memory leak detection

        IFDEF MEM_DEBUG

                display "MEMORY LEAK DETECTION ON..."

PagesAllocd:    ds      256,0

DetectLeaks     macro
                ld      hl,PagesAllocd
                break
                endm

        ELSE

DetectLeaks     macro
                endm

        ENDIF

;;----------------------------------------------------------------------------------------------------------------------
