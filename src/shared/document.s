;;----------------------------------------------------------------------------------------------------------------------
;; Document
;; Contains 48K of tokens
;;----------------------------------------------------------------------------------------------------------------------

; TODO: Dynamic allocation of pages for document

kDocStart       equ     6
kNumDocs        equ     16              ; kNumDocs+1 is maximum number of documents supported

;;----------------------------------------------------------------------------------------------------------------------
;; Document meta-data structure

                STRUCT Document

Length          dw      0               ; Length of document (0 = not used)
Pages           ds      7               ; Pages allocated for document
Flags           db      0               ; Document flags (bit 0 = dirty)

// Editor state
TopAddr         dw      0               ; Document address of line at top of screen
TopLine         dw      0               ; Line number of line at top of screen (0-based)
Addr            dw      0               ; Document address of current line
Line            dw      0               ; Current line number being edited (0-based)
Col             db      0               ; Current column being edited
ReqCol          db      0               ; Required column when moving vertically through document ($ff = not set)
BlockStart      dw      0               ; Line number of where block starts (when EXT+SPACE was pressed)
BlockEnd        dw      0               ; Line number of last line in block (before or after start)
InBlock         db      0               ; Set to 1 if in block mode

                ENDS

                ASSERT 0 == Document.Length     ; some code depends on this (and there's no "search string" to catch them all)

;;----------------------------------------------------------------------------------------------------------------------
;; Gobal data

FNamesPage      db      0               ; Page to hold 32 256-byte filenames
MainDocName     ds      256             ; Filename for current document
MainDoc         Document { $CCCC }
OtherDocs       ds      Document*kNumDocs,$CC   ; Data for other documents
                ASSERT  OtherDocs == MainDoc + Document

FNamesUserPage  equ     kNumDocs+1      ; We use the FNamesPage to hold some more filenames, they start here.  Pass this
                                        ; equate into docName to get HL to point to the address (and B the old page at
                                        ; MMU 4).

;;----------------------------------------------------------------------------------------------------------------------
;; docInit
;; Initialise the document system
;;

docInit:
                ld      hl,0
                ld      (MainDoc.Length),hl     ; docNew will create a new document now

                ; Set up the file names page.  This will also be used as a dummy page that is after each document
                ; so we don't have to keep writing code to check for end of document as we look into a 16K window of
                ; the document.
                call    allocPage
                ret     c
                ld      (FNamesPage),a

                ; Clear the FNamesPage
                call    readPage4
                push    af
                ld      a,(FNamesPage)
                page    4,a
                ld      hl,$8000
                ld      bc,$2000
                call    memclear
                pop     af
                page    4,a

                ; Reset the document data structures.  Setting the length of a document to 0 signifies that the slot
                ; is not used
                ld      hl,MainDoc
                ld      bc,Document*(kNumDocs+1)
                jp      memclear        ; memclear must return CF=0 (it does currently)

;;----------------------------------------------------------------------------------------------------------------------
;; docTryClose
;; Attempt to close the document (will be stopped if dirty or there are no documents)
;;
;; Output:
;;      CF = 1 if no more documents or current document is dirty
;;
;; Affects:
;;      AF, HL
;;

docTryClose:
                ld      hl,MainDoc.Flags
                bit     0,(hl)
                scf
                ret     nz              ; Return CF=1 if dirty

                ;  |
                ; fallthrough into docClose
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; docClose
;; Close current document and free up its pages
;;
;; Output:
;;      CF = 1 if no document to close
;;

docClose:
                call    docExists
                ret     c
                push    bc,de,hl

                ld      b,6
                ld      hl,MainDoc+Document.Pages

                ; Free up the pages
.l1:
                ldi     a,(hl)
                call    freePage
                djnz    .l1

                ; Copy following docs up
                ld      de,MainDoc
                ld      hl,OtherDocs
                ld      bc,Document*kNumDocs
                call    memcpy

                ; Clear last slot
                ld      hl,MainDoc + (Document * kNumDocs)
                xor     a
                ld      (hl),a
                inc     hl
                ld      (hl),a

                ; Copy filenames up
                ld      a,1
                call    docName         ; HL = document name (slot 1) and B = old page for MMU4
                push    bc

                ; Copy filename 1 into current filename
                ld      de,MainDocName
                ld      bc,256
                call    memcpy

                ; Copy filename 2..kNumDocs-1 to filename 1..kNumDocs
                ld      de,hl
                inc     h
                ld      b,kNumDocs-1
                call    memcpy

                pop     af
                page    4,a

                and     a
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docDone
;; Shutdown the document system
;;

docDone:
                ; Close all the documents
                call    docClose
                jr      nc,docDone

                ; Clear the file names page
                ld      a,(FNamesPage)
                call    freePage
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docDirty
;; Set the dirty flag of current document.
;;

docDirty:
                push    hl
                ld      hl,MainDoc.Flags
                set     0,(hl)
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docConstruct
;; Create a new document ready for editing with given filename in FileName buffer and given 48K of pages.
;; This function WILL NOT fill in the length.  This function will also page in the document.
;;
;; Input:
;;      FileName buffer = filename to give document
;;
;; Output:
;;      CF = 1 if document list is full or out of memory for document pages
;;

docConstruct:
                ;;
                ;; Insert a new slot at MainDoc by sliding all the other slots down.  BUT... makes sure we have room
                ;; first.
                ;;

                call    docFullList
                ret     c                               ; list of docs is full, can't add new one

                ;;
                ;; Allocate 48ki for document content
                ;;

                ld      b,6
                ld      hl,.newPages
                call    allocPages
                ret     c                               ; OOM while allocating doc pages

                ;;
                ;; Insert slot at beginning
                ;;

                ld      hl,MainDoc
                ld      de,OtherDocs
                ld      bc,kNumDocs*Document
                call    memcpy_r

                ;;
                ;; Move filenames up and initialise current document filename.
                ;;

                ld      a,(FNamesPage)
                page    4,a
                ld      hl,addrMMU4
                ld      de,addrMMU4+256
                ld      bc,(kNumDocs-1)*256
                call    memcpy_r

                ex      de,hl                           ; DE = 1st slot in filenames page
                ld      hl,MainDocName
                ld      bc,256
                call    memcpy

                call    docSetName

                ;;
                ;; Initialise document state
                ;;

                ld      de,MainDoc.Pages
                ld      hl,.newPages
                ld      bc,6
                ldir

                ex      de,hl
                ld      a,(FNamesPage)                  ; A = page to be used for dummy page
                ldi     (hl),a
                ldi     (hl),b                          ; MainDoc.Flags = 0 (not dirty)

                ; Initialise the editor state
                ld      hl,kDocStart
                ld      (MainDoc.TopAddr),hl            ; ptr to the first line (no content there yet!)
                ld      (MainDoc.Addr),hl
                xor     a
                ld      (MainDoc.Col),a
                ld      h,a
                ld      l,a
                ld      (MainDoc.TopLine),hl
                ld      (MainDoc.Line),hl
                ld      (MainDoc.BlockStart),hl
                ld      (MainDoc.BlockEnd),hl
                ld      (MainDoc.InBlock),a
                dec     a
                ld      (MainDoc.ReqCol),a

                jp      pageDoc

.newPages       ds      6

;;----------------------------------------------------------------------------------------------------------------------
;; docSetName
;; Copy the filename into the current document (no modification of filename is desired on doc-API level)
;;

docSetName:
                push    bc,de,hl
                ld      bc,256
                ld      hl,FileName
                ld      de,MainDocName
                call    memcpy
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docExists
;; Return CF = 1 if no document exists
;;

docExists:
                scf
                push    af,hl
                ld      hl,MainDoc.Length
.checkLength
                ldi     a,(hl)
                or      (hl)
                pop     hl
                jr      z,.noDoc

                pop     af
                ccf
                ret
.noDoc:
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docFullList
;; Return CF = 1 if there's no more space to add another document
;;

docFullList:
                or      a                               ; CF=0 for "no document" case (not full)
                push    af,hl
                ld      hl,MainDoc + kNumDocs*Document + Document.Length
                jr      docExists.checkLength

;;----------------------------------------------------------------------------------------------------------------------
;; docProcessOdnSize
;; Checks if total size of ODN file looks valid, and calculates amount of 8ki pages to accomodate such size.
;;
;; Input:
;;      DEHL = size of the file
;;
;; Output:
;;      when file size is invalid:
;;          CF = 1
;;      when file size is valid:
;;          CF = 0
;;          HL = file size
;;          B  = amount of 8ki pages for such size
;;
;; Affects:
;;      AF, BC, DE, HL
;;

                ; extra entry point for situations when size of opened file should be checked
docProcessOdnSize.fSize:
                call    fGetSize
                ret     c

docProcessOdnSize:
                ld      a,d
                or      e
                add     a,$FF
                ret     c                       ; DE <> 0 -> large file
.ignoreDE:
                ld      de,-$c001
                add     hl,de
                ret     c                       ; HL > $c000 -> large file
                scf                             ; subtract extra +1 to detect zero size
                sbc     hl,de
                ret     c                       ; CF=1 (HL=-1) when size == 0 -> empty file
                inc     hl
                ; File size <= $c000 - preserve it and calculate number of 8ki pages
                push    hl
                call    alignUp8K               ; HL = $2000, $4000, ..., $c000
                ld      a,h
                pop     hl
                swap
                rrca                            ; CF=0 as by-product
                ld      b,a                     ; B = alignUp8K(filesize)/$2000 (number of pages)
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docNew and docNewNoName
;; Attempt to create a new empty document read for editing
;;
;; Input:
;;      FileName buffer = filename of new document (docNew)
;;      none (docNewNoName - enforces "<No Name>" name)
;;
;; Ouput:
;;      CF = 1 if out of memory
;;

docNewNoName:
                ld      hl,FileName
                ld      (hl),0
docNew:
                ;;
                ;; Allocate new pages and construct the document from those pages
                ;;
                call    docConstruct
                ret     c

                ld      de,0
                ld      hl,.empty_doc
                ld      bc,.empty_doc.l
                ldir                                    ; LDIR to get DE pointing after the document content
                ex      de,hl
                ld      (MainDoc.Length),hl             ; ptr beyond the last byte

                ; Fill the rest of the document with crap
.l1:
                inc     hl
                ld      a,h
                cp      $c0
                jr      z,.done
                ld      a,r
                ld      (hl),a
                jr      .l1

                ; Set dirty flag if not "no name" document
.done:
                ld      a,(MainDocName)
                and     a
                call    nz,docDirty                     ; preserves CF=0
                ret

                ; A blank document will be:
                ;
                ; "ODS", <version>      Version number is a single byte
                ; $FF,$00               Marks beginning of file
                ; $00                   Blank Line
                ; $FF                   Marks end of file

.empty_doc:     ; blank document template ; is reused also to check integrity
                db      "ODS",0,$ff,0,0,$ff
.empty_doc.l    equ     $ - .empty_doc

;;----------------------------------------------------------------------------------------------------------------------
;; docCheckIntegrity
;; Checks MainDoc for valid length, header, start and EOF marker
;;
;; Output:
;;      CF = 1 when document doesn't seem to be legal ODS with version 0
;;      CF = 0 document is OK
;;
;; Affects:
;;      AF
;;

docCheckIntegrity:
                push    hl,de,bc
                call    .impl
                pop     bc,de,hl
                ret
.impl:
                ld      de,0
                ld      hl,(MainDoc.Length)
                call    docProcessOdnSize
                ret     c                               ; doc has invalid size
                ; check for EOF mark at end of data
                call    pageDoc
                dec     hl                              ; HL = size-1
                ld      a,(hl)
                sub     $FF
                ret     c                               ; CF=1 when there was no $FF marker at (size-1)
                dec     hl
                cp      (hl)                            ; F = 0 sub (HL)
                ret     c                               ; CF=1 when there was no $00 EOL at (size-2)
                ; check for "ODS" header, version number and start-marker [$FF,$00]
                ; TODO: when more ODN versions will exist, this check need to be extended
                ; (to memcmp only for "ODS" tag and $FF,$00 marker, and process version byte extra)
                ld      hl,docNew.empty_doc
                ld      de,0
                ld      bc,6
                call    memcmp
                scf
                ret     nz                              ; doc beginning looks wrong
                ccf
                ret                                     ; CF=0 -> all OK

;;----------------------------------------------------------------------------------------------------------------------
;; docFindLine
;; Will find the line with the given line number.  Will page in the document.
;;
;; Input:
;;      BC = 16-bit line number
;;
;; Output:
;;      HL = Address of start of line.
;;      CF = 1 if line not found
;;

docFindLine:
                push    bc

                call    pageDoc
                ld      hl,kDocStart
.next_line:
                ; Check to see if we've found the line
                ld      a,b
                or      c
                jr      z,.done

                call    docNextLine
                ; And repeat
                dec     bc
                jr      nc,.next_line   ; CF=1 when there are no more lines

.done: 
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docInsertSpace and docInsertSpaceOneByte
;; Insert space into the document and move everything else up.
;;
;; Input:
;;      HL = beginning of area to insert space
;;      BC = amount of bytes to insert
;;
;; Output:
;;      CF = 1 if not enough room
;;
;; Affects:
;;      AF
;;      docInsertSpaceOneByte also: BC = 1
;;

docInsertSpaceOneByte:
                ld      bc,1
docInsertSpace:
                call    pageDoc
                ld      a,b
                or      c
                ret     z                       ; BC == 0, nothing to do (but CF=0)
                push    de,hl

                ex      de,hl
                ld      hl,(MainDoc.Length)     ; HL = end of doc
                add     hl,bc                   ; HL = final length
                ld      a,$40
                add     a,h
                jr      c,.end                  ; Not enough room (H >= $C0)

                ld      (MainDoc.Length),hl     ; Store final length
                ex      de,hl                   ; HL = where to insert, DE = end of work-area
                call    insertSpace

                ; Outputs CF=0 because HL+BC was <= DE (for valid HL, BC and document)
.endWithDirty:
                call    docDirty

.end:           pop     hl,de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docRemoveSpace
;; Remove space from the document and move everything after it down.
;;
;; Input:
;;      HL = beginning of area to remove
;;      BC = amount of bytes to remove
;;
;; Output:
;;      CF = 1 if not enough bytes in document
;;
;; Affects:
;;      AF
;;

docRemoveSpace:
                call    pageDoc
                ld      a,b
                or      c
                ret     z                       ; BC == 0, nothing to do (but CF=0)
                push    de,hl
                ld      de,(MainDoc.Length)
                call    removeSpace
                jr      c,docInsertSpace.end    ; BC was too large (no dirty flag)
                ex      de,hl
                sbc     hl,bc           ; CF=0 comes from removeSpace (for valid args)
                ld      (MainDoc.Length),hl
                jr      docInsertSpace.endWithDirty     ; set dirty flag + pop HL,DE, CF=0

;;----------------------------------------------------------------------------------------------------------------------
;; docNextLine
;; Find the address of the next line.
;;
;; Input:
;;      HL = start of current line
;;
;; Output:
;;      HL = start of next line
;;      CF = 1 if there is no next line.  Then HL is unchanged
;;
;; Affects:
;;      AF
;;

docNextLine:
                call    pageDoc
                push    bc,hl

                ; Find the end of the line
                xor     a
                ld      b,a
                ld      c,a
                cpir                ; HL points beyond the next zero byte (21T per char)

                inc     a
                add     a,(hl)      ; CF=1 when the next byte is $FF (EOF marker)
                jr      c,.eof
                pop     bc,bc       ; throw away old HL, restore BC
                ret

.eof:           pop     hl,bc       ; restore old HL
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docPrevLine
;; Find the address of the previous line.
;;
;; Input:
;;      HL = start of current line
;;
;; Output:
;;      HL = start of previous line
;;      CF = 1 if there is no previous line.  Then HL is unchanged
;;
;; Affects:
;;      AF
;;

docPrevLine:
                call    pageDoc

                dec     hl          ; Go to end of previous line (this should be null)
                dec     hl          ; Go to last character
                ld      a,1
                add     a,(hl)
                jr      c,.bof      ; CF=1 when BOF marker is found, return restored HL

                ; Find the beginning of previous line
                xor     a
.l0:
                cp      (hl)
                dec     hl
                jp      nz,.l0      ; (23T per char, CPDR would be 21T, but needs BC setup+restore)
                ; HL points ahead of null-terminator of previous line, CF=0, do HL+=2 and return

.bof:
                inc     hl
                inc     hl
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; docReplaceLine
;; Replace a line with another
;;
;; Input:
;;      HL = line to replace (destination)
;;      DE = line to replace with (src)
;;      BC = length of tokenised line
;;
;; Output:
;;      CF = 1 if not enough memory to replace line
;;
;; Affects:
;;      AF
;;

docReplaceLine:
                call    pageDoc
                call    memcmp
                call    nz,docDirty     ; mark dirty when source/destination is not identical on first BC bytes
                
                ; Don't exit early when identical, the HL old content may be longer and match only beginning
                push    hl,bc
                xor     a
                inc     bc              ; account for null-term and for replacing with BC=0 src
                cpir
                dec     hl              ; back at null-term || where the length would reach

                ; If null terminator was hit in document line too soon, insert required space
                call    z,docInsertSpace    ; insert keeps the old null-terminator at (HL) as well (for strlen)

                ; Check for opposite case, requiring removal of remaining part of line
                call    nc,strlen           ; returns BC=0 for same space or after docInsertSpace
                call    nc,docRemoveSpace   ; if CF=1 from docInsertSpace, preserve it + skip remove

                ; Copy the new source data into adjusted space
                pop     bc,hl
                ex      de,hl
                call    nc,memcpy       ; copy only when there was no problem reported by Insert/Remove space
                ex      de,hl
                ret                     ; dirty flag should be already set by docRemoveSpace

;;----------------------------------------------------------------------------------------------------------------------
;; docNewLine
;; Create a new line after the given line
;;
;; Input:
;;      HL = line to add a new line to
;;
;; Output:
;;      HL = start of new line
;;      CF = 1 if error occurs (HL undefined)
;;
;; Affects:
;;      AF
;;

docNewLine:
                call    pageDoc
                call    strEnd      ; can't use docNextLine, this inserts \n also ahead of EOF
                inc     hl
.insert1B:
                push    bc
                call    docInsertSpaceOneByte
                pop     bc
                ret     c
                xor     a
                ld      (hl),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docNewLineAbove
;; Create a new line above the given line
;;
;; Input:
;;      HL = line to add a new line before
;;
;; Output:
;;      HL = start of new line
;;      CF = 1 if error occurs (HL undefined)
;;
;; Affects:
;;      AF
;;

docNewLineAbove:
                call    pageDoc
                jr      docNewLine.insert1B ; Insert null terminator before start of line

;;----------------------------------------------------------------------------------------------------------------------
;; docDeleteLine
;; Delete the line pointed to by HL
;;
;; Input:
;;      HL = line to delete
;;
;; Output:
;;      HL = following line (i.e. HL unchanged) unless line deleted was last, then this points to the last line
;;      CF = 1 if last line was deleted and HL changed.
;;
;; Affects:
;;      AF
;;

docDeleteLine:
                push    bc
                call    pageDoc
                call    strlen
                inc     bc              ; include the null terminator
                call    docRemoveSpace  ; delete the line
                pop     bc
                ld      a,(hl)
                inc     a               ; EOF?
                ret     nz              ; CF=0 from docRemoveSpace

                ; EOF reached
                call    docPrevLine
                jr      c,docNewLineAbove   ; Cannot go to previous lines - there are no previous lines! -> insert one
                scf
                ret                     ; Did move to previous line, report new HL with CF=1

;;----------------------------------------------------------------------------------------------------------------------
;; docInsertLine
;; Insert a line into the document
;;
;; Input:
;;      HL = line to insert before
;;      DE = line to insert
;;
;; Output:
;;      CF = 1 if there isn't enough space in the document
;;

docInsertLine:
                call    pageDoc
                push    bc
                ex      de,hl
                call    strlen          ; BC = length of line to insert
                inc     bc              ; include the null terminator
                ex      de,hl
                call    docInsertSpace
                ex      de,hl
                call    nc,memcpy       ; if insert succeed, copy new line into new space
                ex      de,hl
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docSave
;; Save the current document
;;
;; Input:
;;
;; Output:
;;      CF = 1 if error occurs
;;

docSave:
                call    .preservePage4

                push    bc,de,hl
                ld      hl,MainDocName
                ld      de,FileName
                call    strcpy

                ;;
                ;; Open the file for saving
                ;;

                call    fOpenSave
                jr      c,.done_cf

                ;;
                ;; Write out the document
                ;;

                ld      hl,(MainDoc.Length)
                call    docProcessOdnSize.ignoreDE      ; B = numer of pages to save, HL = size
                ld      de,MainDoc.Pages                ; Page indicies for the document
.next_page:
                ldi     a,(de)                          ; Load next page index
                page    4,a                             ; Page 8K into MMU4
                push    bc
                ld      bc,$2000
                or      a
                sbc     hl,bc                           ; size -= $2000
                jr      nc,.full_page                   ; remaining size >= 0 -> save 8ki
                ; remaining size is less than 8ki, save remaining amount
                add     hl,bc
                ld      bc,hl                           ; BC = remanining bytes to save
.full_page:
                push    hl
                ld      hl,addrMMU4
                call    fWrite
                pop     hl,bc
                jr      c,.error_close
                djnz    .next_page

                ld      hl,MainDoc.Flags
                res     0,(hl)                          ; clear dirty flag
.done:
                call    fClose
                and     a                               ; enforce CF=0
.done_cf:
                pop     hl,de,bc
.old_page+3:    page    4,-1                            ; SMC storage of old MMU4 page
                ret

.preservePage4:
                call    readPage4
                ld      (.old_page),a
                ret

.error_close:
                call    fClose
                scf
                jr      .done_cf

;;----------------------------------------------------------------------------------------------------------------------
;; docLoad
;; Load a new document into current document, whose filename is at FileName.
;;
;; Input:
;;      FileName = filename to load
;;
;; Output:
;;      CF = 1 if loading error occurred
;;

docLoad:
                call    docSave.preservePage4

                push    bc,de,hl

                ;;
                ;; Open for loading and make sure the size is <= 48K
                ;;

                call    fOpenLoad
                jr      c,docSave.done_cf

                ; Check to see if the file we're loading is <= 48K, but not empty, get pages to load
                call    docProcessOdnSize.fSize
                jr      c,docSave.error_close

                ;;
                ;; Create the document in list and allocate pages for it
                ;;

                push    hl,bc                   ; preserve file size and number of pages
                call    docConstruct
                pop     bc,hl
                jr      c,docSave.error_close   ; document list is full or OOM

                ;;
                ;; Set document [expected] size (HL) and load content into B MainDoc.Pages
                ;;

                ld      (MainDoc.Length),hl     ; set expected size
                ld      de,MainDoc.Pages
                call    docLoadPages.loadContent
                call    nc,docCheckIntegrity    ; if loaded OK, check data integrity
                jr      nc,docSave.done         ; if it is valid ODN file, keep it

                ; load-content or integrity check failed, docClose the new document and return with error
                call    docClose
                jr      docSave.error_close

;;----------------------------------------------------------------------------------------------------------------------
;; docLoadPages
;; Load a document temporarily ONLY using the pages required to keep it in memory.
;;
;; Input:
;;      HL = pages array (to allocate and write to, remaining [up to 6] are zeroed)
;;      FileName = file to load
;;
;; TODO: Return error code in A to distinguish between OOM and loading errors.
;;

docLoadPages:
                call    docSave.preservePage4

                push    bc,de,hl

                ; clear the pages array first (also makes it easier to release in case of fRead error)
                ld      bc,6
                call    memclear

                ; Attempt to open the file and get its size
                push    hl
                call    fOpenLoad
                pop     hl
                jr      c,docSave.done_cf       ; Could not open file

                ; Check to see if the file we're loading is <= 48K, but not empty, get pages to load
                push    hl
                call    docProcessOdnSize.fSize ; B = number of pages
                pop     hl
                jr      c,docSave.error_close

                ; Load the content
                call    allocPages
                jr      c,docSave.error_close   ; Out of memory
                push    hl
                ex      de,hl
                call    .loadContent
                pop     hl
                jr      nc,docSave.done         ; loaded OK, finish

                ; deallocate the pages and end with error
                ld      b,6
                call    freePages
                jr      docSave.error_close

; input: DE = allocated pages array, B = pages to load, file is already open
; return: CF=1 when fRead error, CF=0 when all pages were read
; modifies: MMU4 mapping, AF, DE, BC, HL
.loadContent:
                ldi     a,(de)
                page    4,a                     ; Page in an 8K section of document to MMU4

                push    bc
                ld      hl,addrMMU4
                ld      bc,$2000
                call    fRead
                pop     bc
                ret     c

                djnz    .loadContent
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docInfo
;; Get information on document index n (0..kNumDocs)
;;
;; Input:
;;      A = doc index
;;
;; Output:
;;      HL = Document structure
;;

docInfo:
                ASSERT OtherDocs == MainDoc + Document
                push    de
                ld      e,a
                ld      d,Document
                mul
                ld      hl,MainDoc
                add     hl,de
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docName
;; Grab address to document name.  The filename page will be swapped into MMU 4.
;;
;; Input:
;;      A = doc index
;;
;; Output:
;;      HL = document name
;;      B = old page number at MMU 4
;;

docName:
                push    af

                ; pre-calculate HL address for non-main case
                add     a,-1 + high addrMMU4
                ld      h,a
                ld      l,0

                ; Swap in page (irregardless of whether index = 0)
                call    readPage4
                ld      b,a
                ld      a,(FNamesPage)
                page    4,a

                pop     af
                and     a
                ret     nz              ; non-main document, return address from MMU4 block
                ld      hl,MainDocName  ; main document
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docSwap
;; Swap 2 documents in memory
;;
;; Input:
;;      B = document index 1 (0..16)
;;      C = document index 2 (0..16)
;;

docSwap:
                ld      a,b
                cp      c
                ret     z               ; Document indices are equal - no need to swap

                push    de,hl,bc

                ; Swap document info
                ld      a,b
                call    docInfo
                ex      de,hl
                ld      a,c
                call    docInfo         ; DE/HL = document info for two documents

                ld      bc,Document
                call    memswap

                ; Swap document names
                pop     bc
                push    bc

                ld      a,b
                call    docName
                push    bc              ; Store old page number at MMU 4
                ex      de,hl
                ld      a,c
                call    docName         ; DE/HL = document name for two documents

                ld      bc,256
                call    memswap

                pop     bc              ; Restore page number
                ld      a,b
                page    4,a

                pop     bc,hl,de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docFind
;; Given a name pointed to by HL, find the index of the document if it exists.
;; TODO: make this case insensitive
;;
;; Input:
;;      HL = filename
;;
;; Output:
;;      A = document index
;;      CF = 1 if not found
;;

docFind:
                push    bc,de,hl
                ld      de,MainDocName
                call    strcmp
                jr      nz,.not_main

                ; It's the main document
                xor     a
                jr      .done

.not_main:
                call    readPage4
                ld      b,a                     ; B = old page at MMU 4
                ld      a,(FNamesPage)
                page    4,a
                ld      c,1                     ; C = document index
                ex      de,hl                   ; DE = filename
                ld      hl,addrMMU4
.l0:
                ld      a,h
                cp      $80+kNumDocs            ; Past limit of document count?
                jr      nc,.not_found           ; Yes, not found
                ld      a,(hl)
                inc     hl
                or      (hl)                    ; Length == 0?
                jr      z,.not_found            ; Yes, not found
                dec     hl

                call    strcmp
                jr      z,.found

                inc     c
                inc     h
                jr      .l0

.not_found:
                scf
.found:
                ; CF = 1 for not found, 0 for found.  The next instructions do not affect CF
                ld      a,b
                page    4,a
                ld      a,c

.done
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

