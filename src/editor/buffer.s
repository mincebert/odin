;;----------------------------------------------------------------------------------------------------------------------
;; Contains functions to manage the editing buffer
;;
;; Buffer is 256 bytes long (BufferLineLen is length 0..255 without null terminator), but editor may display less of it.
;;----------------------------------------------------------------------------------------------------------------------

BufferLineLen   db      0               ; Current length of line (computed by bufferFromDoc)

;;----------------------------------------------------------------------------------------------------------------------
;; bufferFromDoc
;; Detokenise a document line into the editing buffer
;;
;; Input:
;;      HL = start of document line
;;
;; Affects:
;;      AF, BC
;;

bufferFromDoc:
                push    de,hl
                call    pageDoc
                ld      de,EditorBuffer
                call    detokenise              ; DE = end of buffer
                ld      a,e
                ld      (BufferLineLen),a
                pop     hl,de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferToDoc
;; Replace the line in a document with the tokenised version in the editing buffer
;;
;; Input:
;;      HL = start of line to replace in document
;;
;; Output:
;;      CF = 1 if not enough memory in document to complete operation
;;

bufferToDoc:
                push    bc,de,hl
                call    pageDoc
                ld      hl,TokenisedBuffer
                ld      de,EditorBuffer
                call    tokeniseLine
                ;TODO return value in B could be used to detect+report missing closing quote

                ; Replace the document line
                ld      de,TokenisedBuffer              ; Start of buffer
                and     a
                sbc     hl,de
                ld      bc,hl                           ; BC = Length of tokenised buffer
                pop     hl                              ; HL = line to replace in document
                call    docReplaceLine
                pop     de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferClampPos
;; Adjust a given length to make sure it doesn't go over BufferLineLen
;;
;; Input:
;;      A = position to check
;;
;; Output:
;;      A = clamped position
;;

bufferClampPos:
                push    hl
                ld      hl,BufferLineLen
                cp      (hl)                    ; position to check <= length?
                jr      c,.ok
                ld      a,(hl)
.ok:
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferOverwriteChar
;; Overwrite a character into the editing buffer.  If at end of line then bufferInsertChar is used instead, which
;; can error.
;;
;; Input:
;;      L = column to overwrite
;;      A = character to overwrite with
;;
;; Output:
;;      CF = 1 if no room in buffer (this can happen if you overwrite at end of line)
;;
;; Affects:
;;      AF'
;;

bufferOverwriteChar:
                push    hl
                ld      h,high EditorBuffer         ; HL = overwrite point
                or      a                           ; enforce CF=0
                ; Are we at end of line? (null-terminatr at HL)
                inc     (hl)
                dec     (hl)
                ; if not, overwrite in-place the old non-null character
                jr      nz,bufferInsertChar.overwrite
                ; if at end, insert the new character into buffer
                pop     hl

                ;  |
                ; fallthrough into bufferInsertChar
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; bufferInsertChar
;; Insert a character into the editing buffer.
;;
;; Input:
;;      L = column to insert
;;      A = character to insert
;;
;; Output:
;;      CF = 1 if no more room in buffer
;;
;; Affects:
;;      AF'
;;

bufferInsertChar:
                ; Check to see if we have room and increase the length line ahead
                exa
                ld      a,(BufferLineLen)
                inc     a
                jr      nz,.has_room

                ; Check to see if the last character (#255) is a space, delete it automatically
                ld      a,(EditorBuffer+254)
                sub     ' '
                cpl                     ; A = $FF + ZF=1 when the character is space
                jr      z,.has_room     ; continue as if there was room (the last space will get lost)

                ; report no-room error
                exa
                scf
                ret

.has_room:      ; update the current buffer length with +1 or $FF
                ld      (BufferLineLen),a

                push    hl,de,bc

                ; make room (using A as end of buffer -> handles last space removal!)
                ld      h,high EditorBuffer         ; HL = insert point
                ld      d,h
                ld      e,a                         ; DE = live end of buffer without null-term
                ld      bc,1
                xor     a
                ld      (de),a                      ; enforce new null-terminator after insert (for "last-space" path)
                exa
                call    insertSpace
                pop     bc,de
                jr      c,.error

.overwrite:
                ; Insert character in new space
                ld      (hl),a
                call    docDirty

.error:
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferDeleteChar
;; Delete a character at a given point.  Checks to see if under a null terminator and if so, this is a no-op.
;;
;; Input:
;;      L = offset into buffer to delete
;;
;; Output:
;;      ZF = 1 when there are no more chars left in the buffer from position L
;;

bufferDeleteChar:
                ld      a,(BufferLineLen)
                cp      l
                ret     z

                dec     a
                ld      (BufferLineLen),a

                call    docDirty

                push    hl,de,bc
                ld      h,high EditorBuffer           ; HL = character to delete
                ld      de,EditorBuffer+$100
                ld      bc,1
                call    removeSpace
                pop     bc,de,hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
