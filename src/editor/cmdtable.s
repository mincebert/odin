;;----------------------------------------------------------------------------------------------------------------------
;; Editor's command table
;;----------------------------------------------------------------------------------------------------------------------

;; This table maps keycodes to functions

CmdTable00:
                ; 00
                dw      cmdDoNothing            ; 
                dw      editorCmdExit           ; EDIT
                dw      cmdDoNothing            ; CAPS LOCK
                dw      editorCmdTab            ; TRUE VIDEO
                dw      cmdDoNothing            ; INV VIDEO
                dw      editorCmdLeft           ; LEFT
                dw      editorCmdDown           ; DOWN
                dw      editorCmdUp             ; UP
                dw      editorCmdRight          ; RIGHT
                dw      cmdDoNothing            ; GRAPH
                dw      editorCmdBackspace      ; DELETE
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorCmdNewLine        ; ENTER
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; 10
                dw      cmdDoNothing            ; SYM+W
                dw      cmdDoNothing            ; SYM+E
                dw      cmdDoNothing            ; SYM+I
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorCmdCancel         ; BREAK
                dw      cmdDoNothing            ; SYM+SPACE
                dw      cmdDoNothing            ; SHIFT+ENTER
                dw      cmdDoNothing            ; SYM+ENTER
                dw      cmdDoNothing            ; 

CmdTable80:
                ; 80
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorCmdNewLineAbove   ; EXT+ENTER
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; 90
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; A0
                dw      editorCmdBlockMode      ; EXT+SPACE
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; B0
                dw      editorCmdDelete         ; EXT+0
                dw      editorCmdSwitch         ; EXT+1
                dw      cmdDoNothing            ; EXT+2
                dw      editorCmdStartDoc       ; EXT+3
                dw      editorCmdEndDoc         ; EXT+4
                dw      editorCmdHome           ; EXT+5
                dw      editorCmdPageDown       ; EXT+6
                dw      editorCmdPageUp         ; EXT+7
                dw      editorCmdEnd            ; EXT+8
                dw      cmdDoNothing            ; EXT+9
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; C0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; D0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; E0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; EXT+A
                dw      cmdDoNothing            ; EXT+B
                dw      editorCmdCopy           ; EXT+C
                dw      editorCmdDupLine        ; EXT+D
                dw      editorCmdDeleteToEnd    ; EXT+E
                dw      cmdDoNothing            ; EXT+F
                dw      cmdDoNothing            ; EXT+G
                dw      cmdDoNothing            ; EXT+H
                dw      cmdDoNothing            ; EXT+I
                dw      cmdDoNothing            ; EXT+J
                dw      cmdDoNothing            ; EXT+K
                dw      cmdDoNothing            ; EXT+L
                dw      cmdDoNothing            ; EXT+M
                dw      cmdDoNothing            ; EXT+N
                dw      printChar.overwrite     ; EXT+O

                ; F0
                dw      cmdDoNothing            ; EXT+P
                dw      cmdDoNothing            ; EXT+Q
                dw      cmdDoNothing            ; EXT+R
                dw      editorCmdSave           ; EXT+S
                dw      cmdDoNothing            ; EXT+T
                dw      cmdDoNothing            ; EXT+U
                dw      editorCmdPaste          ; EXT+V
                dw      editorCmdClose          ; EXT+W
                dw      editorCmdCut            ; EXT+X
                dw      cmdDoNothing            ; EXT+Y
                dw      cmdDoNothing            ; EXT+Z
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
