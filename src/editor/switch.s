;;----------------------------------------------------------------------------------------------------------------------
;; Document switching system
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; editorCmdSwitch
;; Enters the document switching mode
;;
;; Call editorSwitch.done if you change editor any other way
;;

editorCmdSwitch:
                call    cursorOff
                call    editorLeaveLine

                ld      hl,(OtherDocs + Document.Length)
                ld      a,h
                or      l
                ret     z                       ; checks if there are at least 2+ documents open

                call    pageVideo

                ;;
                ;; Render the list-box with all documents
                ;;

                bchilo  kScreenHeight-2,0
                call    calcAddr
                ld      c,kInkSwitch << 1
                xor     a
.row:
                call    switchDrawRow
                jr      nc,.row
                ld      l,a                     ; L = number of documents in list

                ;;
                ;; Main loop for switching interface
                ;;

                ld      b,1                     ; B = EditorSwitchY
.main_loop:
                ld      c,kInkSwitchCur<<1
                call    switchPaintRow

                ld      c,kInkSwitch<<1
                call    waitKey

                cp      VK_UP
                jr      z,.up
                cp      VK_DOWN
                jr      z,.down
                cp      VK_BREAK
                jr      z,.done
                cp      VK_EXT1
                jr      z,.done
                cp      VK_ENTER
                jr      nz,.main_loop

                ;;
                ;; Set current document
                ;; Bubble sort required document to slot 0
                ;;

                inc     b                       ; B = EditorSwitchY + 1 (to handle == 0)
                jr      .l0_entry
.l0:
                ld      c,b
                dec     c
                call    docSwap
.l0_entry:
                djnz    .l0

                ; Redraw the editor
.done:
                call    editorEnter
                call    editorEnterLine
                ei
                call    cursorOn
                ret

.down:
                call    switchPaintRow
                dec     b

                ; Check for wrap
                jp      p,.main_loop            ; new entry is fine (>= 0), keep it
                ld      b,l
                djnz    .main_loop              ; wrap around from top by B = L-1

                ; *HERE* unreachable in regular case (but ".up:" handler is next in case of major bug)

.up:
                call    switchPaintRow
                inc     b

                ; Check for wrap
                ld      a,b
                sub     l
                jr      c,.main_loop            ; new entry is fine, keep it
                ld      b,a
                jr      .main_loop              ; wrap around from bottom by B = 0

;;----------------------------------------------------------------------------------------------------------------------
;; switchDrawRow
;; Draw a line in the switch menu
;;
;; Input:
;;      A = document index
;;      HL = screen position
;;      C = colour
;;
;; Output:
;;      CF = 0: HL = screen position for next menu line, ++A
;;      CF = 1: no more lines (A is preserved == number of docs in list, HL undefined)
;;
;; Affects:
;;      B, DE
;;

switchDrawRow:
                cp      kNumDocs+1
                ccf
                ret     c                       ; A is already kNumDocs+1 (beyond list) -> no more

                ; check Document.Length != 0

                ex      de,hl
                call    docInfo
                push    af
                ldi     a,(hl)
                or      (hl)
                jr      z,.no_more

                ; render space or asterisk based on dirty-flag

                add     hl,Document.Flags-1
                bit     0,(hl)
                ld      a,' '
                jr      z,.space
                ld      a,'*'
.space:
                ldi     (de),a                  ; only char written, color later

                ; Render filename

                pop     af
                call    docName                 ; B = old MMU4, HL = filename
                push    af
                ex      de,hl                   ; DE = filename, HL = screen

                ldi     (hl),c                  ; add color to space/asterisk

                push    bc
                call    editorDrawFileName      ; does also fill remaining chars
                pop     bc
                ld      a,b
                page    4,a                     ; restore MMU4 mapping
                pop     af

                add     hl,-2*(80+42)
                add     a,1                     ; ++docIndex and CF=0
                ret

.no_more:
                pop     af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; switchPaintRow
;; Colours a line in the switch menu
;;
;; Input:
;;      B = current Y line selected (0 = bottom of listbox, N-1 = top of it)
;;      C = colour
;;

switchPaintRow:
                push    bc,hl
                ld      a,kScreenHeight-2
                sub     b
                ld      b,a                     ; screen Y position of selected line
                ld      a,c
                ld      c,0
                call    calcAddr
                ld      b,kEditorFNameLen+2
.l0:
                inc     hl
                ld      (hl),a
                inc     hl
                djnz    .l0

                pop     hl,bc
                ret


