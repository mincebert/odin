;;----------------------------------------------------------------------------------------------------------------------
;; Symbol management
;;
;; Symbols are stored in up to 8 pages (64K total).  Each symbol takes 32 bytes, which allows for 26 character symbols,
;; 1 byte for flags, 1 byte for null terminator, 1 byte for hash, and 2 bytes for address/value.  This allows for 2048 
;; symbols (256 symbols per page):
;;
;;                1         2         3
;;      01234567890123456789012345678901
;;      --------------------------------
;;      HCCCCCCCCCCCCCCCCCCCCCCCCCC0FRVV
;;
;;      H = Hash
;;      C = symbol name character
;;      0 = null terminator
;;      F = flags
;;      R = reserved
;;      V = value
;;
;; TODO: hash string to speed up search (hash is not currently supported)
;;----------------------------------------------------------------------------------------------------------------------

                STRUCT SymInfo

Hash            db      0
Text            ds      26
Null            db      0
Flags           db      0
Reserved        db      0
Value           dw      0

                ENDS

MAX_LEN_SYM     equ     26

LastGlobalSym       ds      MAX_LEN_SYM+1,$00
LenLastGlobalSym    dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; symDone
;; Deallocate all memory used by symbols

symDone:
                ld      a,(NumSymPages)
                and     a
                ret     z
                ld      b,a
                ld      hl,SymPages
.l1:
                ldi     a,(hl)
                call    asmFree
                djnz    .l1
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symInit
;; Initialise the symbol state for a new assembly run.
;;

symInit:
                xor     a
                ld      bc,0
                ld      (NumSymPages),a
                ld      (NumSyms),bc
                ld      (NumLocalSyms),bc
                ld      (LenLastGlobalSym),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symLookUp
;; Look up the value of a symbol
;;
;; Input:
;;      DE = symbol name
;;
;; Output:
;;      CF = 0 if symbol exist
;;      BC = value
;;      DE = address after null terminator
;;
;;      CF = 1 if symbol doesn't exist (DE and HL preserved)
;;
;; Affects:
;;      IX, Alternate registers
;;      MMU0 will be changed
;;

symLookUp:
                push    hl,de
                ex      de,hl           ; DE = symbol name
                call    symFind         ; DE = symbol address
                jr      nc,.error
                ld      a,SymInfo.Value
                add     de,a
                ex      de,hl
                ld      c,(hl)
                inc     l
                ld      b,(hl)          ; BC = value
                pop     hl              ; HL = symbol name
                call    strEnd
                inc     hl
                ex      de,hl           ; DE = end of symbol
                pop     hl
                and     a
                ret
.error:
                pop     de,hl
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symGet
;; Convert a symbol handle into an address and page it in to MMU0
;;
;; Input:
;;      DE = Symbol handle
;;
;; Output:
;;      DE = Address + corresponding MMU0 selected
;;
;; Affects:
;;      AF

symGet:
                push    hl
                ld      hl,SymPages
                ld      a,d
                add     hl,a
                ld      a,(hl)
                page    0,a             ; Page in page that contains the symbol

                ld      d,SymInfo
                mul     de              ; DE = address of symbol
                pop     hl
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; symGetLength
;; Get the length of the symbol and check to see it isn't too long
;;
;; Input:
;;      HL = symbol start
;;
;; Output:
;;      HL = symbol start
;;      BC = symbol length
;;      CF = 1 if symbol length OK
;;

symGetLength:
                ; Calculate the length of the symbol
                call    strlen          ; BC = length

                push    hl
                ld      hl,-(MAX_LEN_SYM+1)
                add     hl,bc
                ccf
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symAdd
;; Add a symbol to the symbol table
;;
;; Input:
;;      HL = symbol
;;
;; Output:
;;      if no error:
;;      DE = symbol handle (D = page #, E = symbol # in page)
;;      HL = advanced after null terminator
;;
;;      if error:
;;      error is displayed, output registers are undefined ; TODO: Ped: shouldn't this do something more stable, like DE = -1?
;;
;; Affects:
;;      AF, AF', BC
;;      MMU0 will be changed
;;

symAdd:
                call    symGetLength
                jr      nc,.too_long

                ; Do we need to allocate a new page for the symbol entries?
                ld      a,(NumSyms)     ; We have 256 symbols per 8K page, so if LSB is 0, we need new page
                and     a               ; Reached end of symbol page?  Need to add a new page
                jr      nz,.no_alloc

                ; Allocate a new page
                ld      a,(NumSymPages)
                cp      8
                jr      nc,.table_full
                ld      de,SymPages
                add     de,a            ; DE = address to store page # when allocated
                inc     a
                exa
                call    asmAlloc
                jr      c,.oom

                ld      (de),a          ; Store page #
                exa                     ; Get number of sym pages allocated
                ld      (NumSymPages),a

.no_alloc:
                ; Find the address to store the symbol
                ld      de,(NumSyms)    ; Get new symbol handle
                push    de              ; Store the symbol handle
                inc     de              ; Increment the number of symbols
                ld      (NumSyms),de
                dec     de
                call    symGet          ; DE = symbol address

                ; Copy the symbol name
                push    de              ; Store start of symbol table entry
                inc     e               ; Skip hash value
                inc     c               ; Include the null terminator
                ldir                    ; Copy symbol name into table
                pop     de
                ld      a,SymInfo.Flags
                add     de,a            ; DE = symbol info
                xor     a
                ldi     (de),a          ; Reset the flags
                ldi     (de),a          ; Reset the reserved value
                ldi     (de),a
                ld      (de),a          ; Reset the value to 0
                pop     de              ; Restore the handle to return to caller
                ret

.too_long:
                call    errorPrint
                dz      "SYMBOL TOO LONG"
                ret

.table_full:
                call    errorPrint
                dz      "MAXIMUM NUMBER OF SYMBOLS REACHED"
                ret
.oom:
                call    errorPrint
                dz      "OUT OF MEMORY"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symFind
;; Search for a symbol and return the symbol address with the symbol slot paged into MMU0
;;
;; Input:
;;      HL = symbol
;;
;; Output:
;;      BC = symbol handle
;;      DE = symbol address
;;      CF = 1 if found
;;
;; Affects:
;;      AF
;;
;; TODO: Encode symbol length in lex line, rather than use null terminator
;;

symFind:
                ; Get the length and check to see if length is greater than maximum
                call    symGetLength
                ret     nc              ; If length is too long then we don't have this symbol in our table

                ld      (SymAddr),hl
                ld      a,(hl)
                cp      '.'
                call    z,symExpandLocal
                ld      hl,(SymAddr)

                ld      bc,(NumSyms)
.next_sym:
                ld      a,b
                or      c
                ret     z               ; No more symbols, so not found

                dec     bc              ; [next] symbol handle to search
                ld      de,bc
                call    symGet
                ; HL = symbol to find, BC = symbol handle, DE = symbol address
                inc     e               ; Skip past hash value
                call    strcmp
                jr      nz,.next_sym
                dec     e
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symCheckSym
;; The assembler calls this when it sees a new label.
;;
;; This allows the symbol table manager to store the last global symbol seen and preprend it to local symbols.
;; It also stores the address of the symbol to process to SymAddr.  This will either be in the assembly tokenised line
;; untouched, or to the last global symbol buffer, which will be the concatenation of the last global symbol seen and
;; the current local symbol.
;;
;; Input:
;;      HL = Symbol
;;
;; Output:
;;      SymAddr = Address of symbol
;;      CF = 1 if invalid local
;;
;; Destroys:
;;      BC (0)
;;

SymAddr         dw      0

symCheckSym:
                call    symGetLength    ; BC = length of symbol
                ret     nc              ; Return if too long, let symAdd take care of the errors

                ld      a,(hl)
                cp      '.'
                jr      z,symExpandLocal

                ; Copy global sym into buffer

                push    de,hl
                ld      (LenLastGlobalSym),bc
                ld      de,LastGlobalSym
                inc     c
                ldir
                pop     hl,de
                ld      (SymAddr),hl
                ret

                ; Concatenate given symbol after last global symbol buffer
symExpandLocal:
                ;; INPUT:
                ;;      HL = local symbol
                ;;      BC = length of local symbol
                ;;
                ;; OUTPUT:
                ;;      SymAddr = address of local symbol
                ;;      CF = 1 if invalid local
                ;;
                push    de
                ld      de,(LenLastGlobalSym)
                ld      a,d
                or      e
                jr      z,.invalid_local    ; There was no previous global symbol

                push    hl
                ld      hl,LastGlobalSym
                add     hl,de
                ex      de,hl
                pop     hl
                push    hl
                inc     c                   ; Include null terminator
                ldir
                ld      hl,LastGlobalSym
                ld      (SymAddr),hl
                pop     hl,de
                ret

.invalid_local:
                pop     de
                scf
                ret
