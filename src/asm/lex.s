;;----------------------------------------------------------------------------------------------------------------------
;; Lexical Analyser
;; Reads a line and converts it into tokens
;;

;; $80+ matches the same tokens from token.s

LineEnd         equ     $

T_SYMBOL        equ     1       ; Next bytes are characters followed by null terminator
T_VALUE         equ     2       ; Next word is the value
T_DOLLAR        equ     3       ; Token for '$'
T_STRING        equ     4       ; Used for strings

T_EXPR          equ     5       ; maximum special expression value + 1

;;----------------------------------------------------------------------------------------------------------------------
;; internal EOL handling code for lex (ahead of main routine to compact JR ranges in main routine)
lex_eol:
                xor     a
                call    writeLex
                ex      de,hl
                call    strEnd
                ex      de,hl
                inc     de
                ld      hl,AsmBuffer
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Lexical analyser
;;
;; Input:
;;      DE = input
;;
;; Output:
;;      AsmBuffer contains lexicons
;;      DE = points to start of next line in document
;;      CF = 1 if error occurs
;;      HL = points to line
;;

lex:
                ld      hl,AsmBuffer
.l1:
                call    lexSkipWS
                
                ;
                ; Check for EOL-comment
                ;
                cp      ';'
                jr      z,lex_eol

                ;
                ; Check for EOL and tokens
                ;
                ld      a,(de)
                and     a
                jr      z,lex_eol       ; Reached end of line?
                jp      p,.not_token    ; Is it $01..$7F value? (not token)

                call    writeLexAndIncDe; Write token and advance DE
                ret     c
                ; check for optional sub-token alias-index 1..3
                ld      a,(de)
                dec     a
                cp      3
                jr      nc,.l1          ; It's 0 or 4+, process it
                inc     de              ; It's between 1 and 3 inclusive, skip this byte
                jr      .l1

.not_token:
                ;
                ; Check for decimal digits
                ;
                ld      bc,0            ; BC = current value for value-parsers
                call    isDigit
                jr      nc,.decimal_entry

                ;
                ; Check for labels
                ;
                call    isIdentChar     ; Label?
                jr      c,.not_id
                ld      a,T_SYMBOL
                call    writeLex
                ret     c
.sym_loop:
                ld      a,(de)
                call    isIdentChar
                jr      c,.endsym
                call    writeLexAndIncDe
                ret     c
                jr      .sym_loop
.endsym:
                cp      ':'
                jr      nz,.write_zero
                inc     de              ; Ignore colon
.write_zero:
                xor     a
                jr      .write

                ;
                ; Check for hexadecimal digits and current PC
                ;
.not_id:
                cp      '$'
                jr      nz,.not_hex
                inc     de
                ld      a,(de)
                call    isHexDigit
                jr      c,.dollar
.hex_loop:
                jr      c,.end_value
                push    hl
                ld      hl,bc
                add     hl,hl
                add     hl,hl
                add     hl,hl
                add     hl,hl
                ld      bc,hl           ; BC *= 16
                pop     hl
                call    hexChar
                add     bc,a            ; BC = new value

                inc     de
                ld      a,(de)
                call    isHexDigit
                jr      .hex_loop
.dollar:
                ld      a,T_DOLLAR
.write:
                call    writeLex
                ret     c
                jr      .l1

                ;
                ; Parse decimal digits
                ;
.decimal_loop:
                push    hl
                ld      hl,bc
                add     hl,hl
                add     hl,hl
                add     hl,hl           ; HL = BC*8
                add     hl,bc
                add     hl,bc           ; HL = BC*10
                ld      bc,hl
                pop     hl
.decimal_entry:
                sub     '0'
                add     bc,a
                inc     de
                ld      a,(de)
                call    isDigit
                jr      nc,.decimal_loop
.end_value:
                ; BC = value
                ld      a,T_VALUE
                call    writeLex
                call    nc,writeLexWord
                ret     c
                jp      .l1

                ;
                ; Check for binary digits
                ; 
.not_hex:
                cp      '%'
                jr      nz,.not_binary
                ; going straight into main loop, thus accepting also `%_1` format ("feature" for code size)
.loop_binary:
                inc     de
                ld      a,(de)
                cp      '_'
                jr      z,.loop_binary
                call    isIdentChar
                jr      c,.end_value
                sub     '0'             ; A = 0 or 1
                cp      2
                jr      nc,.syntax_error
.is_bit:
                rra                     ; CF = A
                rl      c
                rl      b
                jr      .loop_binary

                ;
                ; Check for strings
                ;
.not_binary:
                cp      '"'
                jr      z,.string_entry
                cp      "'"
                jr      nz,.not_string
.string_entry:
                ld      c,a         ; preserve delimiter
                ld      a,T_STRING
                call    writeLexAndIncDe
                ret     c
.string_loop:
                ldi     a,(de)
                cp      c
                jr      z,.write_zero
                and     a
                jr      z,.error_string
                cp      $21
                ld      b,1
                jr      nc,.l2      ; not space char, write only one
                sub     $22
                cp      $0a-$22
                ret     c           ; $00..$09 input (invalid)
                cpl                 ; A = number of spaces to fill for $0b..$20
                jr      nz,.not_0a_space
                ; Decompress $0a nn sequence
                ldi     a,(de)      ; $0a has count in next byte
.not_0a_space:
                ld      b,a
                ld      a,' '
.l2:            call    writeLex
                ret     c
                djnz    .l2
                jr      .string_loop

                ;
                ; Check for all other characters
                ;
.not_string:
                inc     de
                cp      $20         ; only $20..$7F chars are legal here ($80+ tokens are already processed)
                jr      nc,.write
.syntax_error:
                call    errorPrint
                dz      'SYNTAX ERROR'
                ret

.error_string:
                call    errorPrint
                dz      "UNTERMINATED STRING"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; lexSkipWS
;; Skip WS in the current line
;;
;; Input:
;;      DE = document
;;
;; Output:
;;      A = first non-space document char
;;      DE = address of first non-space char
;;

lexSkipWS:
                ld      a,(de)
                cp      ' '+1
                ret     nc              ; $21+ value -> not WS
                cp      $0a
                ret     c               ; $00..$09 -> not WS
                inc     de
                jr      nz,lexSkipWS    ; $0b..$20 skipped
                inc     de
                jr      lexSkipWS       ; <$0a,count> skipped


;;----------------------------------------------------------------------------------------------------------------------
;; writeLex
;; Write a byte to the Line buffer
;;
;; Input:
;;      HL = write position in line buffer
;;      A = character to write
;;
;; Output:
;;      CF = 0 if successful, otherwise 1 means buffer overrun
;;

writeLexAndIncDe:
                inc     de          ; size-optimising helper doing also ++DE
writeLex:
        IF DEBUG_LEX
                push    af
                call    pageVideo
                call    printHexByte
                call    printSpace
                call    asmPageDoc
                pop     af
        ENDIF
                ld      (hl),a
                and     a           ; clear CF for `ret nz`
                inc     l
                ret     nz
                ; out of memory
                call    errorPrint
                dz      "LINE TOO COMPLEX"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; writeLexWord
;; Write a word to the line buffer
;;
;; Input:
;;      HL = write position in the line buffer
;;      BC = word to write
;;
;; Output:
;;      CF = 0 if successful
;;

writeLexWord:
                ld      a,c
                call    writeLex
                ret     c
                ld      a,b
                jr      writeLex
